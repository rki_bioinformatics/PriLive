PriLive: Privacy-preserving real-time filtering of Illumina reads
=======================================

Description
-----------

PriLive is a privacy-preserving real-time filtering tool that was designed to remove
human reads from Illumina HiSeq (or comparable) data right in the moment when they
are produced. This means, read filtering is finished as soon as the sequencer is
finished. Human sequence information is never completely available at a single point
in time when using PriLive. This enables a new level of genomic privacy for human-
related sequencing procedures. PriLive is suitable for genomic and metagenomic data
sets.   
   
Features:
   
 * ! Run in parallel to the sequencing machine !   
 * ! Remove sequence information from the original base call files !   
 * Support of a foreground reference genome for genomic data   
 * Suitable for metagenomic data by using a null reference genome   
 * High filtering sensitivity by a local alignment approach   
 * High specificity by setting the -e parameter to a maximal edit distance of interest for the foreground alignment


Get PriLive
-----------

Please visit https://gitlab.com/lokat/PriLive.  
   
There you can find the latest version of PriLive, source code, documentation and
examples.